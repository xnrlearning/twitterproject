@can ('edit', $user)
    <a href="/profiles/{{$user->username}}/edit" class="rounded-full border border-gray-300 
    py-2 px-4 text-black text-xs mr-2">Edit Profile</a>
@endcan

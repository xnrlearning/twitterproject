<x-app>
  
    <header class="mb-6 relative">
        
        <div class="relative">
        
            <img class="mb-2" src="/images/placeholderCapa.jpeg" alt="">
            
            <img src="{{$user->avatar}}" alt="" 
            class="rounded-full mr-2 absolute bottom-0 transform 
            -translate-x-1/2 translate-y-1/2"
            width="150" style="left:50%">
        
        </div>
        
        <div class="flex justify-between items-center mb-6">
            
            <div style="max-width: 270px;">
                <h2 class="font-bold text-2xl  mb-0">{{$user->name}}</h2>
                <p class="text-xs mb-1 text-gray-400">{{ $user->username }}</p>
                <p class="text-sm">Joined {{ $user->created_at->diffForHumans() }}</p>
            </div>

            @auth
                <div class="flex">
                
                    <x-edit-profile-button :user="$user"></x-edit-profile-button>
                    
                    <x-follow-button :user="$user" ></x-follow-button> 
                
                </div>    
            @endauth
            

        </div>

        <p class="text-sm">Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            Nulla sequi id veniam, nisi nostrum eveniet dolorem. 
            Voluptate nisi a quae, consequuntur fugiat soluta dicta vel
            harum in, inventore modi veniam!</p>
            
    </header>
    
    @include('_timeline', ['tweets' => $tweets])

</x-app>
<div class="border border-blue-400 rounded-lg px-8 py-6 mb-8">
    <form action="/tweets" method="POST">
       
        @csrf
        <textarea name="body" class="w-full" placeholder="What's happening?" autofocus required></textarea>
        
        <hr class="my-4">            
        
        <footer class="flex justify-between items-center">
            <img src="{{auth()->user()->avatar}}" alt="avatar" 
            class="rounded-full mr-2" width="50" height="50">
            <button type="submit" class="bg-blue-500 hover:bg-blue-600 rounded-lg shadow px-10 text-sm h-10 text-white">Publish</button>
        </footer>                                    
    </form>

    @error('body')
        <p class="mt-3 text-sm text-red-600">{{$message}}</p>
    @enderror

</div>
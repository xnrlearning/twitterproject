<x-master>


<div class="container mx-auto flex justify-center">
    <div class="px-12 py-8 bg-gray-200 border border-gray-300 rounded-lg">
        <div class="col-md-8">
                <div class="font-bold text-lg mb-4">{{ __('Login') }}</div>

                
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="mb-6">
                            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="email">Email</label>
                        
                            <input type="email" class="border border-gray-400 p-2 w-full" id="email" autocomplete="email" name="email" value="{{ old('email') }}" required>
                            
                            @error('email')
                
                                <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                                </span>
                    
                            @enderror
                          
                        </div>

                        <div class="mb-6">
                            <label class="block mb-2 uppercase font-bold text-xs text-gray-700" for="password">Password</label>
                        
                            <input type="password" class="border border-gray-400 p-2 w-full" id="password" name="password" required>
                            
                            @error('password')
                
                                <span class="invalid-feedback" role="alert">
                                   <strong>{{ $message }}</strong>
                                </span>
                    
                            @enderror
                          
                        </div>

                        <div class="mb-6">
                            <div>                        
                                <input type="checkbox" class="mr-1" id="remember" {{old('remember') ? 'checked' : ''}} name="remember"  >
                                
                                <label class="uppercase text-xs text-gray-700 font-bold" for="remember">Remember Me</label>
                                
                                @error('remember')
                    
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                        
                                @enderror
                            </div>    
                        </div>

                        <div>
                        
                            <button type="submit" class="bg-blue-400 text-white rounded py-2 px-4 hover:bg-blue-500">Submit</button>
                            
                            <a href="{{ route('password.request') }}" class="text-xs text-gray-700 ml-2">Forgot your Password?</a>        

                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</x-master>

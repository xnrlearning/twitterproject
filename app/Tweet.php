<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Likable;


class Tweet extends Model
{

    use Likable;

    protected $guarded = [];
    
    //Get the user that owns the Tweet
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}

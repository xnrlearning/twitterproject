<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\User;

class ProfilesController extends Controller
{
    public function show(User $user)
    {        
        return view('profiles.show', ['user' => $user, 'tweets' => $user->tweets()->withlikes()->paginate(50)]);
    }

    public function edit(User $user)
    {
        return view('profiles.edit', ['user' => $user]);
    }

    public function update(User $user)
    {
        $attributes = request()->validate([

            'name' => ['string', 'required', 'max:255'],
            'username' => ['string', 'max:255', 'required', 'alpha_dash', Rule::unique('users')->ignore($user)],
            'avatar' => ['file'],
            'email' => ['string', 'email', 'max:255', 'required', Rule::unique('users')->ignore($user)],
            'password' => ['string', 'min:8', 'max:255', 'required', 'confirmed'],
        
        ]);
        

        if(request('avatar')){
            //Path file
            $attributes['avatar'] = request('avatar')->store('avatars');
        }
        
        
        $user->update($attributes);
        

        return redirect(route('profile', $user));
    }
}
